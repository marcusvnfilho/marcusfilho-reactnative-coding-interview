import { IEmployee } from '../types/employee';

export type ScreenParamsList = {
  Employees: undefined;
  EmployeeDetail: { employee: IEmployee };
};

export type DrawerParamsList = {
  Home: undefined;
  Profile: undefined
};
