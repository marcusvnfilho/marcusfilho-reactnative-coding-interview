import React from "react";

import { createStackNavigator } from "@react-navigation/stack";
import { createDrawerNavigator } from "@react-navigation/drawer";
import {
  EmployeeDetailScreen,
  EmployeesScreen,
  ProfileScreen,
} from "../screens";
import { ScreenParamsList, DrawerParamsList } from "./paramsList";

const Stack = createStackNavigator<ScreenParamsList>();

const Drawer = createDrawerNavigator<DrawerParamsList>();

function MainNavigator() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerTintColor: "#6C3ECD",
        headerTitleAlign: "center",
      }}
    >
      <Stack.Screen component={EmployeesScreen} name="Employees" />
      <Stack.Screen component={EmployeeDetailScreen} name="EmployeeDetail" />
    </Stack.Navigator>
  );
}

export function DrawerNavigator() {
  return (
  <Drawer.Navigator>
    <Drawer.Screen component={MainNavigator} name="Home" />
    <Drawer.Screen component={ProfileScreen} name="Profile" />
  </Drawer.Navigator>
  )
}
