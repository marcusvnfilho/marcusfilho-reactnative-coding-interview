import React, { useEffect, useMemo } from "react";
import { FlatList, View, RefreshControl } from "react-native";

import { LoadingIndicator, SafeAreaView } from "../../components";
import { Employee } from "../../components/Employee";
import { useListPersons } from "../../hooks/persons";
import { IEmployee } from "../../types/employee";

import styles from "./styles";

export function EmployeesScreen() {
  const { data, error, isLoading, refetch, fetchNextPage, isFetchingNextPage } =
    useListPersons();

  const allData = useMemo(() => data?.pages.flatMap((d) => d.data), [data]);

  useEffect(() => {
    if (error) {
      console.error(error);
    }
  }, [error]);

  return (
    <SafeAreaView>
      <FlatList<IEmployee>
        refreshControl={
          <RefreshControl onRefresh={refetch} refreshing={isLoading} />
        }
        keyExtractor={(item) => item.email}
        data={allData}
        renderItem={({ item }) => <Employee item={item} />}
        onEndReachedThreshold={0.2}
        onEndReached={() => fetchNextPage()}
        ListFooterComponent={
          isLoading || isFetchingNextPage ? <LoadingIndicator /> : <></>
        }
        ItemSeparatorComponent={() => <View style={styles.separator} />}
      />
    </SafeAreaView>
  );
}
