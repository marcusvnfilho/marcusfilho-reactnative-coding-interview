import { StyleSheet, Platform } from "react-native";

export default StyleSheet.create({
  employeeItem: {
    margin: 10,
    padding: 10,
    borderStyle: "solid",
    borderColor: "gray",
    borderRadius: 8,
    borderWidth: 1,
    justifyContent: Platform.select({ ios: 'space-around' , android: 'flex-start'}),
    flexDirection: Platform.select({ ios: 'row' , android: 'column'})
  },
  employeeInfoRow: {
    flexDirection: "row",
    marginBottom: 4,
  },
  employeeInfoRowLabel: {
    marginRight: 8,
    fontWeight: "bold",
  },
});
